//
//  PrivacySettingsCell.h
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacySettingsCell : UITableViewCell

//methods

/**
 @author Vladyslav Bedro
 
 Method fill cell with information
 */
- (void) fillCellWithTitle: (NSString*) infoTitle
           withDescription: (NSString*) descriptionText
           withSwitchState: (BOOL)      switchState;

@end
