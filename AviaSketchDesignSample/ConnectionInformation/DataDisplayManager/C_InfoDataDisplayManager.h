//
//  C_InfoDataDisplayManager.h
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

//protocols
#import "DataDisplayManagerC_InfoOutput.h"

@interface C_InfoDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

//methods

/**
 @author Vladyslav Bedro
 
 Method initializes and binds components of the MVC pattern
 */
- (instancetype) initWithOutput: (id<DataDisplayManagerC_InfoOutput>) output;

@end
