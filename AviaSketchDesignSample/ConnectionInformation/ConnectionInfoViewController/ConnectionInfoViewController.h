//
//  ConnectionInfoViewController.h
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

//protocols
#import "DataDisplayManagerC_InfoOutput.h"

@interface ConnectionInfoViewController : UIViewController <DataDisplayManagerC_InfoOutput>

//properties
@property (weak, nonatomic) id<DataDisplayManagerC_InfoOutput> output;

@end
