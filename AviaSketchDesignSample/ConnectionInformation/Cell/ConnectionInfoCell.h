//
//  ConnectionInfoCell.h
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConnectionInfoCell : UITableViewCell

//methods

/**
 @author Vladyslav Bedro
 
 Method fill cell with information
 */
- (void) fillCellWithLabel: (NSString*) infoLabel
                 withField: (NSString*) inputField
           withButtonState: (BOOL)      showButtonState;

/**
 @author Vladyslav Bedro
 
 Method get input text field of cell
 */
- (UITextField*) getInputField;

@end
