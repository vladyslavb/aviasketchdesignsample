//
//  PrivacySettingsViewController.h
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

//protocols
#import "DataDisplayManagerP_SettingsOutput.h"

@interface PrivacySettingsViewController : UIViewController <DataDisplayManagerP_SettingsOutput>

//properties
@property (weak, nonatomic) id<DataDisplayManagerP_SettingsOutput> output;

@end
