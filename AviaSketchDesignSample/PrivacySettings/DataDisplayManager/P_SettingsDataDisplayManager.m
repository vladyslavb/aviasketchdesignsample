//
//  P_SettingsDataDisplayManager.m
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/31/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "P_SettingsDataDisplayManager.h"

//classes
#import "PrivacySettingsCell.h"

//static variables
static NSString* kSettingsIdentifier = @"PrivacySettingsCell_ID";

static NSString* kTitle       = @"title";
static NSString* kDescription = @"description";
static NSString* kSwitchState = @"switchState";

@interface P_SettingsDataDisplayManager ()

//properties
@property (weak, nonatomic) id<DataDisplayManagerP_SettingsOutput> output;

@property (strong, nonatomic) NSMutableArray* privacySettingsArray;

@end

@implementation P_SettingsDataDisplayManager


#pragma mark - Initialization -

- (instancetype) initWithOutput: (id<DataDisplayManagerP_SettingsOutput>) output
{
    if (self == [super init])
    {
        self.output = output;
    }
    
    return self;
}


#pragma mark - Properties -

- (NSMutableArray*) privacySettingsArray
{
    if (_privacySettingsArray == nil)
    {
        _privacySettingsArray = [NSMutableArray new];
        
        NSDictionary *emailsDictionary = @{kTitle       : @"Notification Emails",
                                           kDescription : @"Receive notification emails to my email "
                                                           "address from this service.",
                                           kSwitchState : @YES
                                         };
        
        NSDictionary *alertsDictionary = @{kTitle       : @"Notification Alerts",
                                           kDescription : @"Receive notification alerts "
                                                           "from this service.",
                                           kSwitchState : @NO
                                            };
        
        NSDictionary *pilotDictionary = @{kTitle       : @"Send Pilot Name",
                                          kDescription : @"Send my pilot name with the "
                                                          "audit information when syncing.",
                                          kSwitchState :  @NO
                                          };
        
        NSDictionary *updateDictionary = @{kTitle       : @"Update device",
                                           kDescription : @"Store and update on my device, "
                                                           "files that may be of a private nature.",
                                           kSwitchState : @YES
                                          };
        
        [_privacySettingsArray addObject: emailsDictionary];
        [_privacySettingsArray addObject: alertsDictionary];
        [_privacySettingsArray addObject: pilotDictionary];
        [_privacySettingsArray addObject: updateDictionary];
    }
    
    return _privacySettingsArray;
}


#pragma mark - UITableViewDataSource methods -

- (NSInteger) numberOfSectionsInTableView: (UITableView*) tableView
{
    return 1;
}

- (NSInteger)     tableView: (UITableView*) tableView
      numberOfRowsInSection: (NSInteger)    section
{
    return 4;
}

- (UITableViewCell*) tableView: (UITableView*) tableView
         cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    PrivacySettingsCell* cell = [tableView dequeueReusableCellWithIdentifier: kSettingsIdentifier
                                                                forIndexPath: indexPath];
    
    NSDictionary* settingsDictionary = self.privacySettingsArray[indexPath.row];
    
    [cell fillCellWithTitle: [settingsDictionary objectForKey: kTitle]
            withDescription: [settingsDictionary objectForKey: kDescription]
            withSwitchState: [[settingsDictionary objectForKey: kSwitchState] boolValue]];
    
    return cell;
}

- (BOOL)            tableView: (UITableView*) tableView
        canMoveRowAtIndexPath: (NSIndexPath*) indexPath
{
    return NO;
}

@end




























