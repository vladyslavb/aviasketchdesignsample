//
//  PrivacySettingsViewController.m
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "PrivacySettingsViewController.h"

//classes
#import <QuartzCore/QuartzCore.h>

#import "P_SettingsDataDisplayManager.h"

@interface PrivacySettingsViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UIButton*    saveButton;
@property (weak, nonatomic) IBOutlet UITableView* settingsTableView;
@property (weak, nonatomic) IBOutlet UIView*      rootContainerView;

//properties
@property (strong, nonatomic) P_SettingsDataDisplayManager* displayManager;

@end

@implementation PrivacySettingsViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self configureUIforController];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Properties -

- (P_SettingsDataDisplayManager*) displayManager
{
    if (_displayManager == nil)
    {
        _displayManager = [[P_SettingsDataDisplayManager alloc] initWithOutput: self];
    }
    
    return _displayManager;
}


#pragma mark - binding UI methods -

- (void) configureUIforController
{
    self.settingsTableView.dataSource = self.displayManager;
    self.settingsTableView.delegate   = self.displayManager;
    
    [self.settingsTableView registerNib: [UINib nibWithNibName: @"PrivacySettingsCell"
                                                        bundle: nil]
                 forCellReuseIdentifier: @"PrivacySettingsCell_ID"];
    
    self.rootContainerView.layer.cornerRadius  = 10;
    self.rootContainerView.layer.masksToBounds = true;
    
    self.saveButton.layer.borderWidth = 0.5f;
    
    UIColor* saveButtonColor = [UIColor colorWithRed: 76.0f  / 255.0f
                                               green: 75.0f  / 255.0f
                                                blue: 106.0f / 255.0f
                                               alpha: 95.0f];
    
    self.saveButton.layer.borderColor = saveButtonColor.CGColor;
    
    self.saveButton.layer.cornerRadius = 10;
    
    [self.navigationController setNavigationBarHidden: YES
                                             animated: NO];
}


#pragma mark - Actions -

- (IBAction) onSaveButtonPressed: (UIButton*) sender
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: @"Main"
                                                         bundle: nil];
    
    PrivacySettingsViewController* pSettingsVC = [storyboard instantiateViewControllerWithIdentifier: @"ConnectionInfoStoryboardID"];
    
    [self.navigationController pushViewController: pSettingsVC
                                         animated: YES];
}

@end























