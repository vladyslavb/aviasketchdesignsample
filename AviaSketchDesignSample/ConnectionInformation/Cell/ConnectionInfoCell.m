//
//  ConnectionInfoCell.m
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ConnectionInfoCell.h"

@interface ConnectionInfoCell() <UITextFieldDelegate>

//outlets
@property (weak, nonatomic) IBOutlet UILabel*     infoLabel;
@property (weak, nonatomic) IBOutlet UITextField* inputField;
@property (weak, nonatomic) IBOutlet UIButton*    showButton;

@end

@implementation ConnectionInfoCell


#pragma mark - Life cycle -

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    self.inputField.delegate = self;
}

- (void) setSelected: (BOOL) selected
            animated: (BOOL) animated
{
    [super setSelected: selected
              animated: animated];
}


#pragma mark - Public methods -

- (void) fillCellWithLabel: (NSString*) infoLabel
                 withField: (NSString*) inputField
           withButtonState: (BOOL)      showButtonState;
{
    self.infoLabel.text         = infoLabel;
    self.inputField.placeholder = inputField;
    self.showButton.hidden      = showButtonState;
    
    if(!self.showButton.hidden)
    {
         self.inputField.secureTextEntry = YES;
    }
}

- (UITextField*) getInputField
{
    return self.inputField;
}


#pragma mark - Actions -

- (IBAction) onShowButtonPressed: (UIButton*) sender
{
    self.inputField.secureTextEntry = !self.inputField.secureTextEntry;
    
    if(self.inputField.secureTextEntry)
    {
        sender.titleLabel.text = @"show";
    }
    else
    {
        sender.titleLabel.text = @"hide";
    }
}


#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn: (UITextField*) textField
{
    [self becomeFirstResponderForNextField: textField];
    
    return YES;
}

- (BOOL) textFieldShouldBeginEditing: (UITextField*) textField
{
    UITableView* tableView;
    
    if ([textField.superview.superview.superview isKindOfClass: [UITableView class]])
    {
        tableView = (UITableView*) textField.superview.superview.superview;
        
    }
    
    CGPoint pointInTable = [textField.superview convertPoint: textField.frame.origin
                                                      toView: tableView];
    
    CGPoint contentOffset = tableView.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    
    [tableView setContentOffset: contentOffset
                       animated: YES];
    
    return YES;
}

- (BOOL) textFieldShouldEndEditing: (UITextField*) textField
{
    [textField resignFirstResponder];
 
    UITableView* tableView;
    
    if ([textField.superview.superview.superview isKindOfClass: [UITableView class]])
    {
        tableView = (UITableView*) textField.superview.superview.superview;
        
    }
    
    if ([textField.superview.superview isKindOfClass: [ConnectionInfoCell class]])
    {
        ConnectionInfoCell* cell = (ConnectionInfoCell*) textField.superview.superview;
        
        NSIndexPath* indexPath = [tableView indexPathForCell: cell];
        
        [tableView scrollToRowAtIndexPath: indexPath
                         atScrollPosition: UITableViewScrollPositionMiddle
                                 animated: TRUE];
    }
    
    return YES;
}


#pragma mark - Internal methods -

- (void) becomeFirstResponderForNextField: (UITextField*) textField
{
    [textField resignFirstResponder];
    
    UITableView* tableView = nil;
    
    if ([textField.superview.superview.superview isKindOfClass: [UITableView class]])
    {
        tableView = (UITableView*) textField.superview.superview.superview;
        
    }
    
    ConnectionInfoCell* cell = nil;
    
    NSIndexPath* indexPath = nil;
    
    if ([textField.superview.superview isKindOfClass: [ConnectionInfoCell class]])
    {
        cell = (ConnectionInfoCell*) textField.superview.superview;
        
        indexPath = [tableView indexPathForCell: cell];
    }
    
    if (indexPath.row != 3)
    {
        NSIndexPath* nextIndexPath = [NSIndexPath indexPathForRow: indexPath.row + 1
                                                        inSection: indexPath.section];
        
        ConnectionInfoCell* nextCell = [tableView cellForRowAtIndexPath: nextIndexPath];
        
        [[nextCell getInputField] becomeFirstResponder];
    }
}

@end





























