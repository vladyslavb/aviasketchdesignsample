//
//  ConnectionInfoViewController.m
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ConnectionInfoViewController.h"

//classes
#import <QuartzCore/QuartzCore.h>

#import "C_InfoDataDisplayManager.h"
#import "PrivacySettingsViewController.h"


@interface ConnectionInfoViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UIButton*    saveButton;
@property (weak, nonatomic) IBOutlet UITableView* mainTableView;
@property (weak, nonatomic) IBOutlet UIView*      rootContainerView;

//properties
@property (strong, nonatomic) C_InfoDataDisplayManager* displayManager;

@end

@implementation ConnectionInfoViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self configureUIforController];
}

#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Properties -

- (C_InfoDataDisplayManager*) displayManager
{
    if (_displayManager == nil)
    {
        _displayManager = [[C_InfoDataDisplayManager alloc] initWithOutput: self];
    }
    
    return _displayManager;
}


#pragma mark - binding UI methods -

- (void) configureUIforController
{
    self.mainTableView.dataSource = self.displayManager;
    self.mainTableView.delegate   = self.displayManager;
    
    [self.mainTableView registerNib: [UINib nibWithNibName: @"ConnectionInfoCell"
                                                    bundle: nil]
             forCellReuseIdentifier: @"ConnectionInfoCell_ID"];
    
    self.rootContainerView.layer.cornerRadius  = 10;
    self.rootContainerView.layer.masksToBounds = true;
    
    self.saveButton.layer.borderWidth = 0.5f;
    
    UIColor* saveButtonColor = [UIColor colorWithRed: 76.0f  / 255.0f
                                               green: 75.0f  / 255.0f
                                                blue: 106.0f / 255.0f
                                               alpha: 95.0f];
    
    self.saveButton.layer.borderColor = saveButtonColor.CGColor;
    
    self.saveButton.layer.cornerRadius = 10;
    
    [self.navigationController setNavigationBarHidden: YES
                                             animated: NO];
}


#pragma mark - Actions -

- (IBAction) onSaveButtonPressed: (UIButton*) sender
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: @"PrivacySettings"
                                                         bundle: nil];
    
    PrivacySettingsViewController* pSettingsVC = [storyboard instantiateViewControllerWithIdentifier: @"PrivacySettingsStoryboardID"];
    
    [self.navigationController pushViewController: pSettingsVC
                                         animated: YES];
}

@end
































