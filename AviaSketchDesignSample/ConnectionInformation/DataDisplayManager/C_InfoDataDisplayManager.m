//
//  C_InfoDataDisplayManager.m
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "C_InfoDataDisplayManager.h"

//classes
#import "ConnectionInfoCell.h"

//static variables
static NSString* kInfoIdentifier = @"ConnectionInfoCell_ID";

static NSString* kInfoLabel           = @"label";
static NSString* kInfoField           = @"field";
static NSString* kInfoShowButtonState = @"showButtonState";

@interface C_InfoDataDisplayManager ()

//properties
@property (weak, nonatomic) id<DataDisplayManagerC_InfoOutput> output;

@property (strong, nonatomic) NSMutableArray* connectionInfoArray;

@end

@implementation C_InfoDataDisplayManager


#pragma mark - Initialization -

- (instancetype) initWithOutput: (id<DataDisplayManagerC_InfoOutput>) output
{
    if (self == [super init])
    {
        self.output = output;
    }
    
    return self;
}


#pragma mark - Properties -

- (NSMutableArray*) connectionInfoArray
{
    if (_connectionInfoArray == nil)
    {
        _connectionInfoArray = [NSMutableArray new];
        
        NSDictionary *userDictionary = @{kInfoLabel           : @"USER ID KEY",
                                         kInfoField           : @"Please enter user id",
                                         kInfoShowButtonState : @NO
                                         };
        
        NSDictionary *libraryDictionary = @{kInfoLabel        : @"LIBRARY ID KEY",
                                         kInfoField           : @"Please enter library id ",
                                         kInfoShowButtonState : @NO
                                         };
        
        NSDictionary *pilotDictionary = @{kInfoLabel          : @"PILOT NAME",
                                         kInfoField           : @"Please enter name",
                                         kInfoShowButtonState : @YES
                                         };
        
        NSDictionary *emailDictionary = @{kInfoLabel          : @"NOTIFICATION EMAIL",
                                         kInfoField           : @"Please enter email",
                                         kInfoShowButtonState : @YES
                                         };
        
        [_connectionInfoArray addObject: userDictionary];
        [_connectionInfoArray addObject: libraryDictionary];
        [_connectionInfoArray addObject: pilotDictionary];
        [_connectionInfoArray addObject: emailDictionary];
    }
    
    return _connectionInfoArray;
}


#pragma mark - UITableViewDataSource methods -

- (NSInteger) numberOfSectionsInTableView: (UITableView*) tableView
{
    return 1;
}

- (NSInteger)     tableView: (UITableView*) tableView
      numberOfRowsInSection: (NSInteger)    section
{
    return 4;
}

- (UITableViewCell*) tableView: (UITableView*) tableView
         cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    ConnectionInfoCell* cell = [tableView dequeueReusableCellWithIdentifier: kInfoIdentifier
                                                               forIndexPath: indexPath];
    
    NSDictionary* infoDictionary = self.connectionInfoArray[indexPath.row];
    
    [cell fillCellWithLabel: [infoDictionary objectForKey: kInfoLabel]
                  withField: [infoDictionary objectForKey: kInfoField]
            withButtonState: [[infoDictionary objectForKey: kInfoShowButtonState] boolValue]] ;
    
    return cell;
}

- (BOOL)            tableView: (UITableView*) tableView
        canMoveRowAtIndexPath: (NSIndexPath*) indexPath
{
    return NO;
}

@end































