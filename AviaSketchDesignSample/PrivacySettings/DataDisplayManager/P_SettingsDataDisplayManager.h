//
//  P_SettingsDataDisplayManager.h
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/31/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

//protocols
#import "DataDisplayManagerP_SettingsOutput.h"

@interface P_SettingsDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

//methods

/**
 @author Vladyslav Bedro
 
 Method initializes and binds components of the MVC pattern
 */
- (instancetype) initWithOutput: (id<DataDisplayManagerP_SettingsOutput>) output;

@end

