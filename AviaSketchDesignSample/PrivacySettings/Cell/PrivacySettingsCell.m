//
//  PrivacySettingsCell.m
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "PrivacySettingsCell.h"

@interface PrivacySettingsCell ()

//outlets
@property (weak, nonatomic) IBOutlet UILabel*  settingTitle;
@property (weak, nonatomic) IBOutlet UILabel*  descriptionSetting;
@property (weak, nonatomic) IBOutlet UISwitch* settingSwitch;


@end

@implementation PrivacySettingsCell

#pragma mark - Life cycle -

- (void) awakeFromNib
{
    [super awakeFromNib];
}

- (void) setSelected: (BOOL) selected
            animated: (BOOL) animated
{
    [super setSelected: selected
              animated: animated];
}


#pragma mark - Public methods -

- (void) fillCellWithTitle: (NSString*) infoTitle
           withDescription: (NSString*) descriptionText
           withSwitchState: (BOOL)      switchState;
{
    self.settingTitle.text         = infoTitle;
    self.descriptionSetting.text   = descriptionText;
    self.settingSwitch.on          = switchState;
}

@end


























