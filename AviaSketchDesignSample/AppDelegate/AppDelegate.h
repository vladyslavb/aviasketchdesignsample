//
//  AppDelegate.h
//  AviaSketchDesignSample
//
//  Created by Vladyslav Bedro on 8/30/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

//properties
@property (strong, nonatomic) UIWindow* window;

@end

